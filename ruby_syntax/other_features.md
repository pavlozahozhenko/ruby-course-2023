# Advanced features:
* Pattern matching
* Static type checking: RBS, TypeProf
* Concurrency/parallelism: Threads, Processes, Ractors, Fibers
