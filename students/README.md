# Project Requirements & Info

### Команди
* Команда має складатися з однієї людини (для інтровертів), з двох, трьох людей
* Придумайте назву своїй команді

### Огранізація
* Зробіть clone репозиторію (якщо ви цього досі не зробили) - `git clone https://gitlab.com/pavlozahozhenko/ruby-course-2023.git`
* Створіть нову гілку з назвою `назва_команди_project` і додайте її до репозиторію. Наприклад:
```
git checkout -b example_team_project
mkdir students/example_team
touch students/example_team/.gitkeep
git add students/example_team/
git commit --message="Added project folder"
git push origin example_team_project
```
* Працюйте над своїм проектом в цій гілці у відповідній теці (students/назва_команди), додавайте коміти
* Коли проект буде готовий - зробіть merge request в бранч master, щоб я зробив code review

### Вимоги до проекта
Проект на вільну тему, але з деякими обмеженнями, а саме:

#### Обов'язкові
* Будь-який Rack framework на Ruby: Ruby on Rails, Roda, Sinatra, Hanami, Cuba, Padrino, etc. За використання не-Rails Rack framework-a (Roda, Sinatra etc) - +10 балів
* Аутентифікація користувачів
* Авторизація, мінімум 2 ролі користувачів з різними правами доступу
* Елементи клієнт-серверної взаємодії зроблені за допомогою Hotwire/Turbo. Або full SPA на якомусь із JS frameworks
* Хоча б одна задача, яка виконується в фоновому (background) режимі (тобто асинхронно в окремому процесі; за допомогою Sidekiq або аналогів)
* Автоматизоване тестування. Як мінімум: unit-тести
* 2+ локалі, не використовувати hardcoded тексти

#### Would be a plus
- Acceptance-тести для main flow (переніс в опціональні, бо не встиг розказати про них на лекції 12.04, розкажу 19.04)
- Функціональні (integration) тести (тести контроллерів), тести background jobs
- Підготовка для запуску в production середовищі, використання production-ready Ruby сервера e.g. Puma, Unicorn, Passenger і production-ready СКБД, e.g. PostgreSQL. "production" environment
- Взаємодія сервер-клієнт через websockets (ActionCable)
- Inbound SMTP server (отримання та процессінґ імейлів) via ActionMailbox *

### Milestones
- "MVP" - 12.04.2023:
  - проект, предметна область
  - команда
  - модель/схема даних з 1-1, 1-n, n-m асоціаціями
  - мінімум 1 CRUD
  - аутентифікація
- "Release" - 19.04.2023: full project requirements implementation

### Загальна інформація
* Тему проекта бажано узгодити зі мною; можу підказати якісь архітектурні рішення, чи варто використати якісь gems і т.д.
* Якщо у вас брак ідей щодо теми проекта - звертайтеся, щось придумаємо
