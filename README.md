# Ruby Course 2023

NaUKMA Ruby on Rails programming course 2023

### Announcements
* Project deadline: 19.04.2023
* Залік - 26.04.2023

### Repository
* [Repository 2023 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2023)

### Course Materials
* [Repository 2022 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2022)
* [Repository 2021 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2021)
* [Repository 2020 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2020)
* [Repository 2019 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2019)
* [Repository 2017 (BitBucket)](https://bitbucket.org/burius/ruby_course_2017)
* [Repository 2016 (BitBucket)](https://bitbucket.org/burius/ror_course)

### The Project
* [Project Requirements](https://gitlab.com/pavlozahozhenko/ruby-course-2023/-/tree/main/students#project-requirements-info)
* Milestone #1 (codename "MVP") - 12.04.2023
* Milestone #2 (codename "Release") - 19.04.2023

### Useful Links
#### Ruby/Rails
* [Official Ruby documentation (core API)](https://ruby-doc.org/3.2.0/)
* [Ruby style guide](https://rubystyle.guide/) by bbatsov
* [Official Rails Guides](https://guides.rubyonrails.org/)
* [Rails API documentation](https://api.rubyonrails.org/)
