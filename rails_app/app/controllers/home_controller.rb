class HomeController < ApplicationController
  protect_from_forgery except: [:put_bar]
  before_action :do_smth, only: [:index, :foo]

  def index
  end

  def foo
    @bar = params[:bar]
  end

  def bar
    render plain: "ID: #{params[:id]}, useful: #{params[:useful]}"
  end

  def put_bar
    render plain: "hello: #{params[:hello]}"
  end

  private

  def do_smth
    puts 'Doing something!!.....'
  end
end
