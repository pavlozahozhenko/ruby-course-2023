class Faculty < ApplicationRecord
  validates :title, presence: true

  def notify_staff
    sleep 10
  end
end
