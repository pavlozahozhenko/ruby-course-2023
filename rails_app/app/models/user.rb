class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable
  validates :email, presence: true, format: { with: /[^@]+@[^@]+/ }

  has_one :address
  has_many :notes, dependent: :destroy
  has_many :roles, class_name: 'User::Role'

  before_save :do_smth
  after_save :report_user_save

  def do_smth
    puts 'before save...'
  end

  def report_user_save
    puts "User was saved #{id} #{full_name}"
  end

  def has_role?(role)
    roles.any? { |r| role == r.role }
  end
end
