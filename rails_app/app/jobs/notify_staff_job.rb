class NotifyStaffJob < ApplicationJob
  queue_as :default
  self.queue_adapter = :sidekiq

  def perform(faculty_id)
    faculty = ::Faculty.find faculty_id
    logger.info "Starting background job for faculty #{faculty.id}"
    faculty.notify_staff
    logger.info "Job's done!"
  end
end
