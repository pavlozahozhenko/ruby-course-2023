json.extract! faculty, :id, :title, :created_at
json.url faculty_url(faculty, format: :json)
