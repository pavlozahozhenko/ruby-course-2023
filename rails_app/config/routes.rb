Rails.application.routes.draw do
  devise_for :users
  resources :faculties
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  root to: 'home#index'

  get '/foo/bar', to: 'home#foo'
  put '/foo/:id/bar', to: 'home#put_bar'
  get '/foo/:id/bar', to: 'home#bar'
  put '/home/foo'
end
