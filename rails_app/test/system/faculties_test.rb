require "application_system_test_case"

class FacultiesTest < ApplicationSystemTestCase
  setup do
    @faculty = faculties(:one)

    @user = users(:sheldon)
    @user.confirm
    sign_in @user
  end

  test "visiting the index" do
    visit faculties_url
    assert_selector "h1", text: "Faculties"
  end

  test "should create faculty" do
    visit faculties_url
    click_on "New faculty"

    fill_in "Title", with: @faculty.title
    click_on "Create Faculty"

    assert_text "Faculty was successfully created"
    click_on "Back"
  end

  test "manager should be able to update Faculty" do
    @user.roles.create(role: 'manager')
    visit faculty_url(@faculty)
    click_on "Edit this faculty", match: :first

    fill_in "Title", with: "#{@faculty.title} forever!!!"
    click_on "Update Faculty"

    assert_text "Faculty was successfully updated"
    click_on "Back"
  end

  test "manager should be able to destroy Faculty" do
    @user.roles.create(role: 'manager')
    visit faculty_url(@faculty)
    click_on "Destroy this faculty", match: :first

    assert_text "Faculty was successfully destroyed"
  end
end
