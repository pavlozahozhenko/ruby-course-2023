require "application_system_test_case"

class AuthenticationTest < ApplicationSystemTestCase
  setup do
    @user = users(:sheldon)
    @user.password = 'password'
    @user.save!
    @user.confirm
  end

  def test_sign_in_existing_user
    visit new_user_session_path
    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_on I18n.t('sign_in.button')
    assert_text 'Hello, world'
  end
end
