require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user = users(:sheldon)
  end
  
  test 'valid user' do
    assert @user.valid?
  end

  def test_has_no_roles
    assert_equal @user.has_role?('employee'), false
  end

  def test_has_employee_role
    @user.roles.create role: :employee
    assert_equal @user.has_role?('employee'), true
  end
end
