class AddEmailToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :email, :string, length: 200, null: false, default: ''
  end
end
