class AddUser < ActiveRecord::Migration[7.0]
  def change
    create_table(:users) do |t|
      t.string :full_name, length: 100, null: false, default: ''
      t.timestamps
    end
  end
end
