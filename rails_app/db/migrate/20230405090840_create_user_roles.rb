class CreateUserRoles < ActiveRecord::Migration[7.0]
  def change
    create_table :user_roles do |t|
      t.string :role, null: false, default: 'employee'
      t.integer :user_id, index: true

      t.timestamps
    end
  end
end
