class CreateFaculties < ActiveRecord::Migration[7.0]
  def change
    create_table :faculties do |t|
      t.string :title, length: 200, default: '', null: false
      t.timestamps
    end
  end
end
