Given /^user exists$/ do
  @user = ::FactoryBot.create :user, email: 'test@example.com'
end

Given /^manager exists$/ do
  @user = ::FactoryBot.create :user, email: 'test@example.com'
  @user.roles.create(role: 'manager')
end

Given /^user is logged in$/ do
  login_as(@user)
end

Given /^user is a manager$/ do
  @user.roles.create(role: 'manager')
end

Given /the user exists with email "(.*)"/ do |email|
  ::FactoryBot.create(:user, email: email)
end

Then /there should exist a user with the name "(.*)"/ do |full_name|
  user = ::User.find_by(full_name: full_name)
  expect(user).not_to be_nil
end

Then /there should be a faculty called "(.*)"/ do |faculty_title|
  @faculty = ::Faculty.find_by(title: faculty_title)
  expect(@faculty).not_to be nil
end
