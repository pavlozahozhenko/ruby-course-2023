Feature: Main flow

  Background:
    Given user exists
    And I am on login page
    When I fill in "user[email]" field with "test@example.com"
    And I fill in "user[password]" field with "password"
    And I click on "Log in"

  @javascript
  Scenario: user logs into the system
    Then I should see text "Hello, world!"

  @javascript
  Scenario: Manager logs into the system and adds a new faculty
    Given user is a manager
    And the user exists with email "to_edit@example.com"
    When I go to root
    And I click on "Faculties"
    And I wait for 2 seconds
    When I click on "New faculty"
    And I fill in "faculty[title]" with "FIN"
    And I click on "Create Faculty"
    Then there should be a faculty called "FIN"
