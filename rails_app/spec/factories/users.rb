FactoryBot.define do
  factory :user do
    full_name { ::FFaker::Name.name }
    email { ::FFaker::Internet.email }
    password { 'password' }
    confirmed_at { ::Time.now }
  end
end
