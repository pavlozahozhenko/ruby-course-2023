require 'rails_helper'

describe User, type: :model do
  subject { ::FactoryBot.create(:user) }

  it 'should have a valid factory' do
    expect(subject).to be_valid
  end

  describe '#has_roles?' do
    it 'should return false if the user has no roles' do
      expect(subject.has_role?('employee')).to be false
    end

    it 'should return true if the user has an employee role' do
      subject.roles.create role: :employee
      expect(subject.has_role?('employee')).to be true
    end
  end
end
