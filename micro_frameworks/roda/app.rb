require 'roda'
require './db/connect'

class App < Roda
  plugin :all_verbs
  plugin :render
  plugin :public

  route do |r|
    r.public
    r.root do
      'Hello from Roda!'
    end
    r.on 'todos' do
      @todos = DB[:todos]

      r.get 'index' do
        @todo_items = @todos.reverse(:date).to_hash_groups(:date)
        view('todos/index')
      end

      r.on Integer do |id|
        r.put 'mark_as_done' do
          @todos.where(id: id)
                .update(is_done: true)
          "Marked todo #{id} as done"
        end
      end
    end
  end
end
