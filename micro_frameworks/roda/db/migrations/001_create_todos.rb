Sequel.migration do
  change do
    create_table(:todos) do
      primary_key :id
      String :text, size: 255
    end
  end
end
