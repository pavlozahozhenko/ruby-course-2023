Sequel.migration do
  change do
    alter_table(:todos) do
      add_column :is_done, FalseClass, default: false
    end
  end
end
